# ANR ECLATS Livrables lot 2



Ce projet Gitlab contient les rapports et logiciels produits dans le cadre du Lot2 (Préparation et encodage des données)du projet ANR ECLATS (Lots 1 et 4). Ces travaux, menés par l'équipe IMAGINE du LIRIS,  s'intègrent au projet ANR ECLATS qui vise dans sa globalité à fournir un outillage numérique innovant permettant le stockage, l’enrichissement sémantique, la visualisation des contenus géolinguistiques mais aussi la création de cartes interprétatives à partir des données extraites de l'ALF 
(Altas Linguistique de la France).

L'objectif de ce lot 2 est la mise en forme optimale des versions numérisées des cartes de l'ALF. Cette mise en forme concerne en premier lieu la qualité des cartes numérisées qui comportent certaines dégradations liées à la qualité de conservation des documents originaux. Mais ce lot 2 se consacre aussi à la décomposition du contenu des cartes en couches sémantiques pour permettre une structuration des données et une compression de l'information, essentielle à un stockage, une exploitation et une diffusion efficaces.

Le lot a été décomposé en deux sous tâches dont les résultats sont disponibles dans les répertoire WP2.3 et WP2.4

![figLivrablesWP2](/uploads/b34b6c9785debd1a0748d5c26be7697b/figLivrablesWP2.png)

- la tâche 2.3 : concerne l'étude des formats de compression, le choix s'étant  finalement porté sur l'utilisation du format SVG pour une représentation des cartes de l'ALF
- la tâche 2.4 : cette tâche fait suite à la tache 2.3 et a comme objectif de proposer un format de stockage des données de l’ALF qui permette une compression importante en ne conservant que les données essentielles des cartes (les écrits et les tracés par exemple) et en supprimant le  reste  (suppression des informations relatives à la texture du papier par exemple).



